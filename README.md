# EiffelOnWasm

We intend to make it possible to run Eiffel apps on the browser so that means using WASM.
The project is divided in 3 stages

- Get all of [liberty eiffel](https://wiki.liberty-eiffel.org/index.php/Liberty_Eiffel_Programming) to compile to wasm.
- Get all of the example projects created by Eiffel Studio (Library, Basic, Web, Windows)
- Profit!

## How this project is executed

We want every step of this experiment to be reproducible and also understandable.
So we have written everything in *literate bash* which means our documentation are at the sametime bash scripts that do most of the work.
All of the artifacts on this project should be reproducible.
most of the experiments happen in `./workdir/` and it is ignored in .gitignore so not to contaminate the repo.

## Table of contents


### First [install_liberty_eiffel](./doc/install_liberty_eiffel.bash.md).

Apart from a redable markdown is a script. you can execute it in your commandd line.
You should only need to run this once.
On the tutorial folder there are several examples, we will get `Hello World` compiling in wasm.

### [compile_hello_world_to_c_to_wasm_no_garbagae_collection](./doc/compile_tutorial_to_wasm.bash.md)

Again it's a script.
Now that we comipled_to_c and then to_wasm with no_garbagecollection.
Once you execute the .bash.md file...

     How to preview:

     cd <YOUR_PATH>/eiffelonwasm/workdir/compilations/hello_world/compile_to_c/no_gc//wasm/"
     python3 -m http.server
     google-chrome http://localhost:8000/hello_world.html

Your page should look like this:
![](./imgs/screenshot_hello_world.png)

### Compile programs in the tutorial folder.

experiments_with_all_the_tutorials.bash.md

I was able to compile several apps in the tutorial folder with garbagecollection on.
the ones that gave me trouble were:

- If it was expecting a user input,
- if it was expecting a command line parameter.
- The ones with ncurses.

ut the ones that always work are the ones that only do calculations and print.

### Todo: try to get ncurses to work.
There are several librariar for ncurses and wasm in github.
let's try to link a couple of those.


### Todo: compile a GUI application

This GUI library works with LibertyEiffel, and the examples look promising.
https://notabug.org/GermanGT/eiffel-iup

## License

AGPL
