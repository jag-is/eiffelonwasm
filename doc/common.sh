###### this are needed to test this script
###### Probably I'll extract them to their own file.
# Get the repos top directory

git_root=$(git rev-parse --show-toplevel)
workdir="$git_root/workdir"

function append_if_not_found(){
    local LINE=$1
    local FILE="${2/#\~/$HOME}"

    if [ ! -e "$FILE" ]; then
        echo "Error: File '$FILE' does not exist." >&2
        exit 1
    fi

    grep -qF -- "$LINE" "$FILE" || echo "$LINE" >> "$FILE"
}

# clean a directory and move there
mkcd() {
    local dir_name="$1"

    if [ -z "$dir_name" ]; then
        echo "Please provide a directory name."
        return 1
    fi

    [ -d "$dir_name" ] && rm -rf "$dir_name"
    mkdir -p "$dir_name" && cd "$dir_name"
}

# Function to assert equality of two values
verify() {
    local actual="$1"
    local expected="$2"
    local message="$3"

    if [ "$actual" != "$expected" ]; then
        echo "Assertion failed: $message"
        echo "Expected: $expected"
        echo "Got: $actual"
        exit 1
    fi
}
