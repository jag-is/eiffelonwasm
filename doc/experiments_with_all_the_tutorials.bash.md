#!/bin/bash

set -xeuo pipefail
IFS=$'\n\t'

source ./common.sh

## compilations that work

### compile hello_worild WITH garbage collection

    bash compile_tutorial_to_wasm.bash.md hello_world.e with_gc

### print_arguments.e with garbage collection

    bash compile_tutorial_to_wasm.bash.md print_arguments.e with_gc

###### It does finish compilation with no errors. but when running it doesn't do anything.
###### I was expecting it would ask for an input parameter.

### fizzbuzz.e

    bash compile_tutorial_to_wasm.bash.md fizzbuzz.e with_gc

###### Works perfectly

### eulero.e

    bash compile_tutorial_to_wasm.bash.md eulero.e with_gc

###### ok

    bash compile_tutorial_to_wasm.bash.md natural_usage.e with_gc

###### ok

    bash compile_tutorial_to_wasm.bash.md downcasting.e with_gc

###### ok

    bash compile_tutorial_to_wasm.bash.md loops.e with_gc

###### ok

## Partiall work

    bash compile_tutorial_to_wasm.bash.md pyramid.e with_gc

###### So so... It doesn't work completely... you type a number... and if you press cancel in the dialogbox it does somethnig.

## compilations that don't work.

###    bash compile_tutorial_to_wasm.bash.md fibonacci.e with_gc
###### it displays an input dialog box (well!!) but it doesn't do anything :(

###    bash compile_tutorial_to_wasm.bash.md test_thread.e with_gc
###### nothing happens just a black screen on the browser

###    bash compile_tutorial_to_wasm.bash.md loops.e with_gc
###### nothing happens just a black screen on the browser

###    bash compile_tutorial_to_wasm.bash.md pyramid2.e with_gc
###### It displays an input dialog but then shows error Nan

###    bash compile_tutorial_to_wasm.bash.md knight.e with_gc
###### It displays an input dialog but then shows error Nan

###    bash compile_tutorial_to_wasm.bash.md spread_illness.e with_gc
###### It displays an input dialog but then shows error Nan

###    bash compile_tutorial_to_wasm.bash.md manifest_notation.e with_gc
###### same problem with the input_dialog
