#!/bin/bash

set -xeuo pipefail
IFS=$'\n\t'

source ./common.sh

## We will compile hello_world without garbage collection. Although this can be changed by calling the script with parameters

    tutorial_name="${1:-hello_world.e}"
    program="${tutorial_name%.e}"
    gc_option="${2:-no_gc}"

## Based on the value of $gc_option, decide the flags and directory structure:

    if [[ "$gc_option" == "no_gc" ]]; then
        compilation_dir="$workdir/compilations/$program/compile_to_c/no_gc"
        compile_flags="-no_gc -no_strip -no_split"
    else
        compilation_dir="$workdir/compilations/$program/compile_to_c/gc"
        compile_flags="-no_strip -no_split"
    fi

## Compile hello world to C and no Garbage Collection

     mkcd "$compilation_dir"
     se compile_to_c $compile_flags "$workdir/liberty-eiffel/tutorial/$tutorial_name"

## Replace gcc in the generated MakeFile

     sed -i \
         -e 's/gcc\b/emcc/g;' \
         -e 's/-x none/-o hello_world.html/g' \
         -e 's/-Xlinker -no-as-needed//g' \
         "./$program.make"

## Let's compile it first in order to get *.o file

     bash "$program.make"

## then compile it with emcc to get .html, .js and .wasm files

     mkdir ./wasm/
     emcc $(ls "${program}"*.o) -o "./wasm/$program.html"

## now you need to preview it just do:

     cat <<END
     How to preview:

     cd "$compilation_dir/wasm/"
     pkill -f "python3 -m http.server" || true
     python3 -m http.server &
     sleep 3
     google-chrome http://localhost:8000/${program}.html

END
