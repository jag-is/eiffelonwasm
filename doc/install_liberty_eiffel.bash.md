#!/bin/bash
set -xeuo pipefail
IFS=$'\n\t'

source ./common.sh

# Clean the current installation... so that everytime I restart this starts from a clean space.

    rm -rf "$workdir"

# Install Liberty Eiffel

##### Based on this guide https://wiki.liberty-eiffel.org/index.php/Getting_Started
##### I'll install by source code since It will probable be needed any way.

## Install dependencies

    sudo apt-get install -y git build-essential castxml libgc-dev

## Clone repo

    mkcd "$workdir"
    git clone --depth=1 git://git.sv.gnu.org/liberty-eiffel.git
    cd "$workdir/liberty-eiffel/"
    ./install.sh -bootstrap

## lets verify liberty-eiffel was installed:

    "$workdir"/liberty-eiffel/target/bin/se --version

## make liberty eiffel available in the session.

    append_if_not_found "export PATH=\$PATH:$workdir/liberty-eiffel/target/bin" "$HOME/.bashrc"

## reload .bashrc

    . ~/.bashrc

## make sure that now se is part of the path

    se --version

## compile hello_world with default parameters

    program='hello_world'
    compilation_dir="$workdir/compilations/$program/standard"
    mkcd "$compilation_dir"
    cd "$workdir/liberty-eiffel/tutorial/"
    se compile "$program.e" -o "$compilation_dir/$program"
    cd "$compilation_dir/"
    "./$program"

## You have successfully installed liberty Eiffel
